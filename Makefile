default :
	@echo "available stages:"
	@${MAKE} --no-print-directory available_stages 2>&1 | sed 's/^/    /'

available_stages : 
	@find [0-9][0-9]-* -name 'ci.stage.*' -printf "%f\n" | sort -u | while read stage ; do echo "make $${stage}" ; done



ci.stage.% :
	find * -maxdepth 0 -type d -name '[0-9][0-9]-*' | sort | while read submodule ; do run-parts --verbose --regex "^$@" $${submodule} ; done
